package math

type Aged interface {
	GetBirthday() string
	SetAge(int)
}
