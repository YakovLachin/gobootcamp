package math

import (
	"time"
	"fmt"
)

const (
	HoursInDay = 24
	DaysInYear = 365
	shortForm = "2006-Jan-02"
)

func CalculateAge(aged Aged)  {
	birthday, err := time.Parse(shortForm, aged.GetBirthday())
	if (err != nil) {
		fmt.Printf("%s \n", err)
		return
	}

	age := time.Since(birthday) / time.Hour / HoursInDay / DaysInYear
	aged.SetAge(int(age))
}
