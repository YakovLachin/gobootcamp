package math

//Расчитывает среднее колчиство
func Average(obj Averagable) {
	res := obj.GetTotal() / obj.GetQty()
	obj.SetAverage(res)
}