package math

type Averagable interface {
	GetQty() int
	GetTotal() int
	SetAverage(average int)
}