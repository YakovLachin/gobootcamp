package main

import (
	"gobootcamp/bowling/player"
	"gobootcamp/bowling/math"
	"gobootcamp/bowling/render/objectRender"
)

func main() {
	name := struct {
		FirstName  string
		SecondName string
	}{
		"mr Badger",
		"ZERG",
	}
	playerData := struct {
		Name           player.Name
		BirthdayDate   string
		Age            int
		QtyPlayedGames int
		TotalPoints    int
		AveragePoints  int
	}{name, "1990-Apr-10", 0, 1, 1, 0}

	var gamer player.Player = player.Player(playerData)

	math.Average(&gamer)
	math.CalculateAge(&gamer)
	objectRender.Render(&gamer)
}
