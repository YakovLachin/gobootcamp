package player

// структура Player
type Player struct {
	Name           Name
	BirthdayDate   string
	Age            int
	QtyPlayedGames int
	TotalPoints    int
	AveragePoints  int
}

//Имя Игрока
type Name struct {
	FirstName  string
	SecondName string
}

//Возваращает количетсво игр
func (p *Player) GetQty() int {
	return p.QtyPlayedGames
}

//Возвращает общее количество очков
func (p *Player) GetTotal() int {
	return p.TotalPoints
}

//Записывает среднее значение за игры
func (p *Player) SetAverage(average int) {
	p.AveragePoints = average
}

//Возвращает структуру данных для рендера.
func (p *Player) GetRenderStruct() map[string]interface{} {
	renderData := map[string] interface {
	}{
		"Имя Игрока" : p.Name,
		"День Рождение игрока" : p.BirthdayDate,
		"Средний балл за игру" : p.AveragePoints,
		"Количество лет": p.Age,
	}

	return renderData
}

func (p *Player) GetBirthday() string {
	return p.BirthdayDate
}

func (p *Player) SetAge(age int) {
	p.Age = age
}