package render

type Renderable interface {
	GetRenderStruct() map[string]interface{}
}