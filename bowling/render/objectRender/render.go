package objectRender

import (
	"gobootcamp/bowling/render"
	"fmt"
)

func Render(r render.Renderable) {
	renderStruct  := r.GetRenderStruct()

	for key, value := range renderStruct {
		fmt.Printf("%v: %v\n", key,value)
	}
}