package moneyconv

var rubRates = map[string] float64 {
	RubCur: 1,
	UsdCur: 50,
}

var usdRates = map[string] float64 {
	RubCur: 0.01724137931,
	UsdCur: 1,
}

var rates = map[string] map[string] float64 {
	RubCur: rubRates,
	UsdCur: usdRates,
}


func Convert(money *Money) {
	rates := getRates(money.InputCurrency)
	rate := rates[money.OutputCurrency]
	money.Rate = rate
	money.ResultQuantity = float64(money.Quantity) * rate
}

func getRates(rate string) map[string] float64  {
	return rates[rate]
}