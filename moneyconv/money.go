package moneyconv

const (
	RubCur string = "RUB"
	UsdCur string = "USD"
)


type Money struct {
	Quantity int
	ResultQuantity float64
	InputCurrency string
	OutputCurrency string
	Rate float64
}
