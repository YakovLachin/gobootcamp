/*
	Package moneyconv предназначен для конвертации денежной валюты

	Usage:

	Запустить через CLI используя флаги amount и rate

	Command line flag syntax:
		-amount // Количество едениц валюты
		-rate   // Курс одной еденицы валюты
*/
package main

import (
	"flag"
	"fmt"
)

func main() {
	var amount, rate int
	flag.IntVar(&amount, "amount", 0, "Флаг amount принимает на вход целые числа.")
	flag.IntVar(&rate, "rate", 0, "Флаг rate принимает на вход целые числа.")
	flag.Parse()

	if amount == 0 || rate == 0 {
		fmt.Println("Пожалуйста, перезапустите программу используя флаги -amount -rate.\n")
	}
	result := amount * rate 
	fmt.Printf("Результат: %d\n", result)
}
