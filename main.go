/*
	Package moneyconv предназначен для конвертации денежной валюты

	Usage:

	Запустить через CLI используя флаги amount и rate

	Command line flag syntax:
		-amount // Количество едениц валюты
		-rate   // Курс одной еденицы валюты
*/
package main

import (
	"flag"
	"fmt"
	"gobootcamp/moneyconv"
)

func main() {
	var moneyData = struct {
		Quantity int
		ResultQuantity float64
		InputCurrency string
		OutputCurrency string
		Rate float64
	} {100,0, moneyconv.RubCur, moneyconv.UsdCur, 0}
	flag.IntVar(&moneyData.Quantity, "amount", 0, "Флаг amount принимает на вход целые числа.")
	flag.StringVar(&moneyData.InputCurrency, "src", moneyconv.RubCur, "Тип Исхоной Валюты по ISO 4217.")
	flag.StringVar(&moneyData.OutputCurrency, "dst", moneyconv.UsdCur, "Тип Исхоной Валюты по ISO 4217.")
	flag.Parse()
	money := moneyconv.Money(moneyData)

	moneyconv.Convert(&money)
	fmt.Printf("Результат: %v\n", money)
}
